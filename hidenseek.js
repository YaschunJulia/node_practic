const fs = require('fs');
const PersonList = require('./personlist.js');

exports.hide = function(path, personsList) {
    return new Promise((resolve,reject) => {
        let rndSart = Math.floor(Math.random() * personsList.length);
        let rndCount = Math.floor(Math.random() * 3 + 1);
        let str = '';
        let hidePerson = new PersonList();

        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }

        for (let i = rndSart; i < Math.min(rndCount + rndSart, personsList.length); i++) {
            str += `${personsList[i].name}|${personsList[i].age}\n`;
            hidePerson.add(personsList[i].name, personsList[i].age);
        }

        fs.writeFile(`${path}/person.txt`, str, 'utf-8', function (err) {
            if (err) reject(err);
            resolve(hidePerson);
        });
    })
}

exports.seek = function(path) {
    return new Promise(function(resolve,reject){
        let findPerson = new PersonList();

        fs.readFile(`${path}/person.txt`, {encoding: 'utf-8'}, (err, data) => {
            if (err) reject(err); ;
            let persons = data.split('\n');
            persons.splice(-1);
            persons.forEach((person) => {
                let arr = person.split('|');
                findPerson.add(arr[0], arr[1]);
            })
            resolve(findPerson);
        }); 
    })
}

const user = required('./user')