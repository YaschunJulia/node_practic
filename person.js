'use strict';

class Person {

  constructor(name, age) {
    this.name = name;
    this.age = age
  }

  show() {
    console.log(`My name is ${this.name} and I'm ${this.age} years old.`);
  }

  valueOf() {
      return this.age;
  }

}

module.exports = Person;