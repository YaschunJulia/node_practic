const { Transform } = require('stream');
const fs = require('fs');
const crypto = require('crypto');
const readStream = fs.createReadStream('person.json');
let hash = crypto.createHash('md5');
const writeStream = fs.createWriteStream('_person.json');

const toHex = new Transform({
    transform(chunk, encoding, callback) {
        hash = crypto.createHash('md5');

        this.push(hash.digest('hex'));
        callback();
    }
});

readStream.pipe(hash).pipe(toHex).pipe(process.stdout);
readStream.pipe(hash).pipe(writeStream);

readStream.on('error', (err) => {
    console.error(err);
});


