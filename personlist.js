const Person = require('./person.js');

class PersonList extends Array{
    constructor(...args) {
        super(...args);
    }

    add(name, age) {
        let person = new Person(name, age);
        this.push(person)
    }

    show() {
        let str = '';
        this.forEach(function(person, index) {
            str += `${index} Name: ${person.name} Age: ${person.age}\n`;
        });
        str += `Count: ${this.length}`;
        console.log(str);
    }

    max() {
        return Math.max.apply( Math, this );
    }
}

module.exports = PersonList;