const fs = require('fs');
const Person = require('./person.js');
const PersonList = require('./personlist.js');
const hidenseek = require('./hidenseek.js');

console.log('--------task 5---------');

let yuliia = new Person('Yuliia', 24);

yuliia.show();

let lost = new PersonList(new Person('Dima', 20), new Person('Sveta', 18), new Person('Tema', 8), new Person('Lena', 48));
let found = new PersonList(new Person('Roma', 22));

lost.show();
found.show();

lost.add('Vanya', 38);
lost.add('Petya', 34);
found.add('Nadya', 56);

lost.show();
found.show();

let person = lost.splice(1, 1);
person[0].show();
found.push(person[0]);

lost.show();
found.show();

console.log(found.max());


console.log('--------task 6---------');

let number = Math.floor(Math.random() * 3 + 2);

for (let i = 1; i <= 10; i++){
    let dir = __dirname + '/0' + i;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    if ( i % number === 0) {
        hidenseek.hide(dir, lost).then((hidePerson) => {
            console.log('Hide in folder /0' + i);
            hidePerson.show();
        });
    }
}

let dir = __dirname + '/field';

hidenseek.hide(dir, lost).then((hidePerson) => {
    console.log('Hide in folder /field');
    hidePerson.show();
    hidenseek.seek(dir).then((findPerson) => {
        console.log('Seek in folder /field');
        findPerson.show();
    })
})


