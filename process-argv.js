const fs = require('fs');
const PersonList = require('./personlist.js');
const hidenseek = require('./hidenseek.js');

if (process.argv.length === 2){
    console.log(`Print one of the command:
    hide [path] [path to personsList] - to hide some persons to the file that specified in path
    seek [path] - to seek hidden persons from file`)
} else {
    if (process.argv[2] === 'hide') {
         fs.readFile(process.argv[4] || 'person.json', {encoding: 'utf-8'}, (err, data) => {
            if (err) reject(err); ;
            let persons = JSON.parse(data).persons;
            console.log(persons);
            let personsList = new PersonList();

            persons.forEach((person) => {
                personsList.add(person.name, person.age);
            })

            hidenseek.hide(process.argv[3], personsList).then((hidePerson) => {
                console.log('Hide');
                hidePerson.show();
            })
        }); 
    } else if (process.argv[2] === 'seek') {
        hidenseek.seek(process.argv[3]).then((findPerson) => {
            console.log('Seek');
            findPerson.show();
        })
    }
}